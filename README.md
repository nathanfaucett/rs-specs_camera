# specs_camera

camera 2d and 3d component for specs

```rust
let mut world = World::new();

let mut dispatcher = Bundler::new(&mut world, DispatcherBuilder::new())
    .bundle(TransformBundle::<f32>::default()).unwrap()
    // CameraBundle adds TransformSystem<T> to CameraSystem<T> deps
    .bundle(CameraBundle::<f32>::default()).unwrap()
    .build();

world.create_entity()
    .with(Transform2D::<f32>::default())
    .with(Camera2D::<f32>::default().with_size(1024, 768))
    .build();

dispatcher.dispatch(&mut world.res);
```
