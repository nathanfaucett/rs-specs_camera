use std::sync::atomic::AtomicBool;

#[inline(always)]
pub fn atomic_bool_new_true() -> AtomicBool {
    AtomicBool::new(true)
}
