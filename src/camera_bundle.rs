use std::marker::PhantomData;
use std::ops::{Add, Div, Mul, Neg, Sub};

use num_traits::{Float, FromPrimitive};
use specs::{WorldExt};
use specs_bundler::{Bundle, Bundler};
use specs_transform::TransformSystem;

use super::{Camera2D, Camera3D, CameraSystem};

#[derive(Debug)]
pub struct CameraBundle<'deps, T> {
    deps: Vec<&'deps str>,
    _marker: PhantomData<T>,
}

impl<'deps, T> Default for CameraBundle<'deps, T> {
    #[inline]
    fn default() -> Self {
        CameraBundle::new(&[])
    }
}

impl<'deps, T> CameraBundle<'deps, T> {
    #[inline]
    pub fn new(deps: &[&'deps str]) -> Self {
        CameraBundle {
            deps: deps.to_vec(),
            _marker: PhantomData,
        }
    }
}

impl<'deps, 'world, 'a, 'b, T> Bundle<'world, 'a, 'b> for CameraBundle<'deps, T>
where
    T: 'static + Send + Sync + Float + FromPrimitive,
    for<'c, 'd> &'c T: Sub<&'d T, Output = T>
        + Add<&'d T, Output = T>
        + Div<&'d T, Output = T>
        + Mul<&'d T, Output = T>
        + Neg<Output = T>,
{
    type Error = ();

    #[inline]
    fn bundle(
        mut self,
        mut bundler: Bundler<'world, 'a, 'b>,
    ) -> Result<Bundler<'world, 'a, 'b>, Self::Error> {
        bundler.world.register::<Camera3D<T>>();
        bundler.world.register::<Camera2D<T>>();

        self.deps.push(TransformSystem::<T>::name());

        bundler.dispatcher_builder.add(
            CameraSystem::<T>::new(),
            CameraSystem::<T>::name(),
            self.deps.as_slice(),
        );

        Ok(bundler)
    }
}
