extern crate mat32;
extern crate mat4;
extern crate num_traits;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate specs;
extern crate specs_bundler;
extern crate specs_transform;
extern crate type_name;

mod atomic_bool_new_true;
mod camera_2d;
mod camera_3d;
mod camera_bundle;
mod camera_system;

pub(crate) use self::atomic_bool_new_true::atomic_bool_new_true;
pub use self::camera_2d::Camera2D;
pub use self::camera_3d::Camera3D;
pub use self::camera_bundle::CameraBundle;
pub use self::camera_system::CameraSystem;
