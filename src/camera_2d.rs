use std::fmt;
use std::ops::{Add, Div, Mul, Neg, Sub};
use std::sync::atomic::{AtomicBool, Ordering};

use mat32;
use num_traits::{Float, FromPrimitive};

use specs::{Component, VecStorage};

use super::atomic_bool_new_true;

#[derive(Serialize, Deserialize)]
pub struct Camera2D<T> {
    size: (usize, usize),
    ortho_size: T,
    projection: [T; 6],
    view: [T; 6],
    #[serde(skip, default = "atomic_bool_new_true")]
    dirty: AtomicBool,
}

impl<T> Component for Camera2D<T>
where
    T: 'static + Sync + Send,
{
    type Storage = VecStorage<Self>;
}

impl<T> fmt::Debug for Camera2D<T>
where
    T: fmt::Debug,
{
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("Camera2D")
            .field("ortho_size", &self.ortho_size)
            .field("projection", &self.projection)
            .field("view", &self.view)
            .finish()
    }
}

impl<T> Default for Camera2D<T>
where
    T: Float + FromPrimitive,
{
    #[inline(always)]
    fn default() -> Self {
        Camera2D {
            size: (512, 512),
            ortho_size: T::from_usize(2).unwrap(),
            projection: mat32::new_identity(),
            view: mat32::new_identity(),
            dirty: AtomicBool::new(true),
        }
    }
}

impl<T> Camera2D<T>
where
    T: Float + FromPrimitive,
    for<'a, 'b> &'a T: Sub<&'b T, Output = T>
        + Add<&'b T, Output = T>
        + Div<&'b T, Output = T>
        + Mul<&'b T, Output = T>
        + Neg<Output = T>,
{
    #[inline(always)]
    pub fn new() -> Self {
        Self::default()
    }

    #[inline]
    pub(crate) fn flag(&self, dirty: bool) {
        self.dirty.store(dirty, Ordering::SeqCst)
    }
    #[inline]
    pub fn is_dirty(&self) -> bool {
        self.dirty.load(Ordering::SeqCst)
    }

    #[inline(always)]
    pub fn width(&self) -> usize {
        self.size.0
    }
    #[inline(always)]
    pub fn height(&self) -> usize {
        self.size.1
    }
    #[inline(always)]
    pub fn size(&self) -> (usize, usize) {
        self.size
    }
    #[inline]
    pub fn with_size(mut self, width: usize, height: usize) -> Self {
        self.size.0 = width;
        self.size.1 = height;
        self
    }
    #[inline]
    pub fn set_size(&mut self, width: usize, height: usize) -> &mut Self {
        self.size.0 = width;
        self.size.1 = height;
        self.flag(true);
        self
    }

    #[inline(always)]
    pub fn ortho_size(&self) -> T {
        self.ortho_size
    }
    #[inline]
    pub fn with_ortho_size(mut self, ortho_size: T) -> Self {
        self.ortho_size = ortho_size;
        self
    }
    #[inline]
    pub fn set_ortho_size(&mut self, ortho_size: T) -> &mut Self {
        self.ortho_size = ortho_size;
        self.flag(true);
        self
    }

    #[inline(always)]
    pub fn projection(&self) -> &[T; 6] {
        &self.projection
    }

    #[inline(always)]
    pub fn view(&self) -> &[T; 6] {
        &self.view
    }

    #[inline]
    pub fn update_view(&mut self, matrix: &[T; 6]) {
        mat32::inv(&mut self.view, matrix);
    }

    #[inline]
    pub fn update_projection(&mut self, force: bool) {
        if self.is_dirty() || force {
            self.flag(false);
            let w = T::from_usize(self.width()).unwrap();
            let h = T::from_usize(self.height()).unwrap();
            let w_gt_h = &w > &h;
            let aspect = if w_gt_h { &w / &h } else { &h / &w };
            let ortho_size_aspect = &self.ortho_size * &aspect;
            let r = if w_gt_h {
                ortho_size_aspect
            } else {
                self.ortho_size
            };
            let l = -&r;
            let t = if w_gt_h {
                self.ortho_size
            } else {
                ortho_size_aspect
            };
            let b = -&t;
            mat32::orthographic(&mut self.projection, &t, &r, &b, &l);
        }
    }
}
